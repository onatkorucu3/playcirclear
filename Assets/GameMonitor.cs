﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class GameMonitor : MonoBehaviour{
	
	#region SINGLETON
	
	public static GameMonitor instance;
    
	void Awake()
	{
		Debug.Log("GameMonitor AWAKE, will send OpenApplication.");
		if (instance != null)
		{
			Destroy(gameObject);
		}
		else
		{
			instance = this;
			DontDestroyOnLoad(gameObject);
		}

		OpenApplication();
	}

	#endregion

	#region ACTIONS
	public event Action<string, DateTime, string, string> OnApplicationOpen;
	public event Action<string, bool> OnUserLogin;
	public event Action<string, int, bool, DateTime, int, string> OnPlayerDataEntered;
	public event Action<DateTime, string, int> OnGameLevelStart;
	public event Action<string, string, int, MimickedEmotion, float> OnFaceDetectionStepCompleted;
	public event Action<string, string, int, float> OnFaceDetectionLevelCompleted;
	public event Action<string, string, int, float> OnFaceDetectionLevelFailed;
	public event Action<string, string, int, string, float> OnClassificationStepCompleted;
	public event Action<string, string, int, string> OnClassificationStepDecayed;
	public event Action<string, string, int, List<string>, List<string>, float> OnClassificationLevelCompleted;
	public event Action<string, string, int, List<string>, List<string>, float> OnClassificationLevelFailed;
	#endregion

	private const string USER_EMAIL = "userEmail";
	private const string LAST_PLAYED_LEVEL = "lastPlayedLevel";
	private const string GAME_ID_TIMESTAMP = "gameIdTimestamp";

	private List<string> grossClassifiedObjectList;
	private List<string> netClassifiedObjectList;
	private List<string> decayedObjectList;
	
	private string userEmail => PlayerPrefs.GetString(USER_EMAIL);
	private string gameIdTimestamp => PlayerPrefs.GetString(GAME_ID_TIMESTAMP);
	private int lastPlayedLevel => PlayerPrefs.GetInt(LAST_PLAYED_LEVEL);

	#region PLAYER_INFO
	private void OpenApplication() {
		//app opened - varsa demografik bilgisi (userID) / generated user id, unique device id, device model
		
		DateTime timestamp = DateTime.Now;

		if (OnApplicationOpen != null)
		{
			OnApplicationOpen(userEmail,
				timestamp,
				SystemInfo.deviceUniqueIdentifier, 
				SystemInfo.deviceModel);
		}
	}
	
	public void UserLogin(string userEmail, bool isFirstLogin) {
		PlayerPrefs.SetString(USER_EMAIL, userEmail);
		PlayerPrefs.Save();

		if (OnUserLogin != null) {OnUserLogin(userEmail, isFirstLogin);}
	}
	
	public void PlayerDataEntered(bool isMale, int siblingCount, DateTime birthDate, int technologyUsageLevel, string healthStatus) {
		//demographic data entered - userID, sibling, sex, birthdate etc.

		if (OnPlayerDataEntered != null) OnPlayerDataEntered(userEmail, siblingCount, isMale, birthDate, technologyUsageLevel, healthStatus);
	}
	#endregion
	
	#region LEVEL_STARTED
	public void GameLevelStart(int levelNumber) {
		//game start - userID, levelID, timestamp, oyunID, duration since app opened?
		//timestamp = gameID;
		
		DateTime timestamp = DateTime.Now;

		grossClassifiedObjectList = new List<string>();
		netClassifiedObjectList = new List<string>();
		decayedObjectList = new List<string>();
		
		SaveLastPlayedLevel(levelNumber);
		
		PlayerPrefs.SetString(GAME_ID_TIMESTAMP, timestamp.ToString());
		PlayerPrefs.Save();

		if (OnGameLevelStart != null) OnGameLevelStart(timestamp, userEmail, levelNumber);
	}
	#endregion

	#region FACE_DETECTION
	public void FaceDetectionStepCompleted(float stepCompletionTime, MimickedEmotion mimickedEmotion)
	{
		if (OnFaceDetectionStepCompleted != null)
			OnFaceDetectionStepCompleted(
				userEmail,
				gameIdTimestamp,
				lastPlayedLevel,
				mimickedEmotion,
				stepCompletionTime);
	}
	
	public void FaceDetectionLevelCompleted(float levelCompletionTime)
	{
		if (OnFaceDetectionLevelCompleted != null)
			OnFaceDetectionLevelCompleted(
				userEmail,
				gameIdTimestamp,
				lastPlayedLevel,
				levelCompletionTime);
	}
	
	public void FaceDetectionLevelFailed(float levelFailTime)
	{
		if (OnFaceDetectionLevelFailed != null)
			OnFaceDetectionLevelFailed(
				userEmail,
				gameIdTimestamp,
				lastPlayedLevel,
				levelFailTime);
	}
	#endregion

	#region CLASSIFICATION
	public void ClassificationStepCompleted(float stepCompletionTime, string classifiedObject)
	{
		grossClassifiedObjectList.Add(classifiedObject);
		Debug.Log("ppp grossClassifiedObjectList first element is:" + grossClassifiedObjectList[0]);
		if (OnClassificationStepCompleted != null)
			OnClassificationStepCompleted(
				userEmail,
				gameIdTimestamp,
				lastPlayedLevel,
				classifiedObject,
				stepCompletionTime);
	}
	
	public void ClassificationStepDecayed(string decayedObject)
	{
		decayedObjectList.Add(decayedObject);
		if (OnClassificationStepDecayed != null)
			OnClassificationStepDecayed(
				userEmail,
				gameIdTimestamp,
				lastPlayedLevel,
				decayedObject);
	}
	
	public void ClassificationLevelCompleted(float levelCompletionTime)
	{
		if (OnClassificationLevelCompleted != null)
			OnClassificationLevelCompleted(
				userEmail,
				gameIdTimestamp,
				lastPlayedLevel,
				grossClassifiedObjectList,
				decayedObjectList,
				levelCompletionTime
			);
		
		decayedObjectList = new List<string>();
		grossClassifiedObjectList = new List<string>();
	}
	
	public void ClassificationLevelFailed(float levelFailTime)
	{
		if (OnClassificationLevelFailed != null)
			OnClassificationLevelFailed(
				userEmail,
				gameIdTimestamp,
				lastPlayedLevel,
				grossClassifiedObjectList,
				decayedObjectList,
				levelFailTime);
		
		decayedObjectList = new List<string>();
		grossClassifiedObjectList = new List<string>();
	}
	#endregion

	private void SaveLastPlayedLevel(int levelNumber) {
		PlayerPrefs.SetInt(LAST_PLAYED_LEVEL, levelNumber);
		PlayerPrefs.Save();
	}
	
}