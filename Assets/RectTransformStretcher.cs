﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RectTransformStretcher : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        RectTransform rt = GetComponent<RectTransform>();
        rt.anchorMin = Vector2.zero;
        rt.anchorMax = Vector2.one;
        rt.sizeDelta = Vector2.zero;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
