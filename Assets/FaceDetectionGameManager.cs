﻿using Unity.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class FaceDetectionGameManager : MonoBehaviour
{
    [SerializeField] private Image _emojiImage;

    private MimickedEmotion _mimickedEmotion;

    [SerializeField, ReadOnly] private Sprite[] _expressionImageList;
    
    [SerializeField] private Sprite[] _emojiList;
    
    int lastUsedEmojiIndex;

    [SerializeField] private float _difficultyLevel;

    private int _emojiIndex;
    private int _score;
    
    [SerializeField] private Image[] _starImageList;
    [SerializeField] private Sprite _successStar;
    [SerializeField] private Sprite _noSuccessStar;
    [SerializeField] private int _maxScore;


    [SerializeField] private GameObject _winPopup;
    
    [SerializeField] private AuthManager _authManager;

    [ReadOnly] private float stepCompletionTime;
    [ReadOnly] private float levelCompletionTime;
    
    [ReadOnly] private float levelFailTime;
    
    private void Awake()
    {
        stepCompletionTime = 0;
        levelCompletionTime = 0;
        levelFailTime = 0;
        
        CloudFaceDetector.OnFaceAnalysisReady += HandleFaceAnalysis;

        _expressionImageList = _emojiList; 
        _emojiIndex = -1;
        _score = 0;

        for (int i = 0; i < _starImageList.Length; i++)
        {
            _starImageList[i].sprite = _noSuccessStar;
        }
    }

    private void Start()
    {
        ChangeShownEmojiWithRandomAnother();
        
        _authManager = FindObjectOfType<AuthManager>();
    }
    
    private void Update()
    {
        stepCompletionTime += Time.deltaTime;
        levelFailTime += Time.deltaTime;
    }

    private void OnDestroy()
    {
        CloudFaceDetector.OnFaceAnalysisReady -= HandleFaceAnalysis;
    }

    void HandleFaceAnalysis(Face face)
    {
        Debug.Log("inside HandleFaceAnalysis");
        
        DecideMimickedEmotion(face);

        if (IsMissionCompleted())
        {
            HandleStepCompleted();
        }
        else
        {
            HandleMissionFailed();
        }
    }

    private void HandleMissionFailed()
    {
        Debug.Log("mission failed");
    }

    private void HandleStepCompleted()
    {
        Debug.Log("mission completed");

        IncreaseScore();

        bool isLevelComplete = _score == _maxScore;

        FillStepCompleteAnalyticsData(isLevelComplete);
        ChangeShownEmojiWithRandomAnother();
    }

    private void FillStepCompleteAnalyticsData(bool isLevelComplete)
    {
        GameMonitor.instance.FaceDetectionStepCompleted(stepCompletionTime, _mimickedEmotion);
        levelCompletionTime += stepCompletionTime;
        stepCompletionTime = 0;
        
        if (isLevelComplete)
        {
            GameMonitor.instance.FaceDetectionLevelCompleted(levelCompletionTime);
            levelCompletionTime = 0;
        }
    }

    private int IncreaseScore()
    {
        if (_score < _maxScore)
        {
            _score++;
        }

        if (_score == _maxScore)
        {
            HandleLevelComplete();
        }
        
        for (int i = 0; i < _score; i++)
        {
            _starImageList[i].sprite = _successStar;
        }

        return _score;
    }

    private void HandleLevelComplete()
    {
        _winPopup.SetActive(true);
    }

    private void ChangeShownEmojiWithRandomAnother()
    {
        while (_emojiIndex == lastUsedEmojiIndex || _emojiIndex < 0) //Get one random at the beginning.
        {
            _emojiIndex = Random.Range(0, _expressionImageList.Length);
        }

        lastUsedEmojiIndex = _emojiIndex;
        _emojiImage.sprite = _expressionImageList[_emojiIndex];
    }

    private void DecideMimickedEmotion(Face face)
    {
        if (face.faceAttributes.emotion.happiness * 100> _difficultyLevel)
        {
            _mimickedEmotion = MimickedEmotion.Happiness;
        }
        
        else if (face.faceAttributes.emotion.sadness  * 100 > _difficultyLevel)
        {
            _mimickedEmotion = MimickedEmotion.Sadness;
        }
        
        else if (face.faceAttributes.emotion.anger * 100 > _difficultyLevel)
        {
            _mimickedEmotion = MimickedEmotion.Anger;
        }
        
        else if (face.faceAttributes.emotion.fear  * 100 > _difficultyLevel)
        {
            _mimickedEmotion = MimickedEmotion.Fear;
        }
        
        else if (face.faceAttributes.emotion.surprise * 100 > _difficultyLevel)
        {
            _mimickedEmotion = MimickedEmotion.Surprise;
        }
        
        else if (face.faceAttributes.emotion.disgust  * 100> _difficultyLevel)
        {
            _mimickedEmotion = MimickedEmotion.Disgust;
        }
        
        else if (face.faceAttributes.emotion.contempt * 100 > _difficultyLevel)
        {
            _mimickedEmotion = MimickedEmotion.Contempt;
        }
        else
        {
            _mimickedEmotion = MimickedEmotion.Neutral;
        }

        Debug.Log("mimicked emotion is: " + _mimickedEmotion);
    }

    public bool IsMissionCompleted(){

        if (_mimickedEmotion == MimickedEmotion.Happiness && _emojiImage.sprite == _expressionImageList[0])
        {
            return true;
        } 
        if (_mimickedEmotion == MimickedEmotion.Sadness && _emojiImage.sprite == _expressionImageList[1])
        {
            return true;
        } 
        if (_mimickedEmotion == MimickedEmotion.Surprise && _emojiImage.sprite == _expressionImageList[2])
        {
            return true;
        }
        if (_mimickedEmotion == MimickedEmotion.Neutral && _emojiImage.sprite == _expressionImageList[3])
        {
            return true;
        }

        return false;
    }

    public void GoBackButton()
    {
        Debug.Log("BACK BUTTON PRESSED IN FACE DETECTION SCENE. levelFailTime is: " + levelFailTime);
        GameMonitor.instance.FaceDetectionLevelFailed(levelFailTime);
        SceneManager.LoadSceneAsync("LevelScene");
    }

}

public enum MimickedEmotion
{
    Happiness,
    Sadness,
    Anger,
    Fear,
    Surprise,
    Disgust,
    Contempt,
    Neutral
}
