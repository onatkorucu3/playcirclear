﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Auth;
using Firebase.Database;
using UnityEngine.UI;

public class AuthManager : MonoBehaviour
{
    //Firebase variables
    [Header("Firebase")]
    public DependencyStatus dependencyStatus;
    public FirebaseAuth auth;    
    public FirebaseUser User;

    //Login variables
    [Header("Login")]
    public InputField emailLoginField;
    public InputField passwordLoginField;
    public Text warningLoginText;
    public Text confirmLoginText;

    //Register variables
    [Header("Register")]
    ///public Text usernameRegisterField;
    public InputField emailRegisterField;
    public InputField passwordRegisterField;
    ///public Text passwordRegisterVerifyField;
    public Text warningRegisterText;

    private DatabaseReference databaseReference;

    private string _sessionEmail;

    void Awake()
    {
        //Check that all of the necessary dependencies for Firebase are present on the system
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                //If they are avalible Initialize Firebase
                InitializeFirebase();
            }
            else
            {
                Debug.LogError("Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
        

    }

    private void Start()
    {
        //SaveDummyData();
    }

    public void SaveDummyData()
    {
        //StartCoroutine(UpdateUserNameAuth("ont"));
        //StartCoroutine(UpdateUserNameDatabase("mont"));
        
        StartCoroutine(UpdatePlayerDemographicData(true, false, DateTimeOffset.Now.ToUnixTimeMilliseconds(), 5, "disleksi, renk körü"));

    }

    private IEnumerator UpdateUserNameDatabase(string _username)
    {
        var DBTask = databaseReference.Child("users").Child("3").Child("username").SetValueAsync(_username);
        
        yield return new WaitForSeconds(1);
        
    }
    
    public void SavePlayerDemographicData(bool isMale, bool hasSibling, DateTime birthday,  int technologyUsageLevel, string healthStatus)
    {
        StartCoroutine(UpdatePlayerDemographicData(isMale, hasSibling, birthday.Ticks, technologyUsageLevel, healthStatus)); 
    }

    private IEnumerator UpdatePlayerDemographicData(bool isMale, bool hasSibling, long birthday, int technologyUsageLevel, string healthStatus)
    {
        /*var DBTask = databaseReference.Child("players").Child(_sessionEmail).Child("isMale").SetValueAsync(isMale);
        databaseReference.Child("players").Child(_sessionEmail).Child("birthday").SetValueAsync(birthday);
        databaseReference.Child("players").Child(_sessionEmail).Child("healthStatus").SetValueAsync(healthStatus);
        databaseReference.Child("players").Child(_sessionEmail).Child("technologyUsageLevel").SetValueAsync(technologyUsageLevel);
        databaseReference.Child("players").Child(_sessionEmail).Child("hasSibling").SetValueAsync(hasSibling);
        */
        
        Debug.Log("entered UpdatePlayerDemographicData");
        IDictionary<string,object> playerDemographicDataDictionary = new Dictionary<string, object>();
        playerDemographicDataDictionary.Add("isMale", isMale);
        playerDemographicDataDictionary.Add("hasSibling", hasSibling);
        playerDemographicDataDictionary.Add("birthday", birthday);
        playerDemographicDataDictionary.Add("technologyUsageLevel", technologyUsageLevel);
        playerDemographicDataDictionary.Add("healthStatus", healthStatus);
        Debug.Log("between");

        
        Firebase.Auth.FirebaseUser user = auth.CurrentUser;
        string uid = user.UserId;
        
        //var DBTask = databaseReference.Child("players").Child(uid).Child("email").SetValueAsync(_sessionEmail);
        
        var DBTask = databaseReference.Child("players").Child(uid).Child("email").SetValueAsync(_sessionEmail);
        databaseReference.Child("players").Child(uid).Child("isMale").SetValueAsync(isMale);
        databaseReference.Child("players").Child(uid).Child("hasSibling").SetValueAsync(hasSibling);
        databaseReference.Child("players").Child(uid).Child("birthday").SetValueAsync(birthday);
        databaseReference.Child("players").Child(uid).Child("technologyUsageLevel").SetValueAsync(technologyUsageLevel);
        databaseReference.Child("players").Child(uid).Child("healthStatus").SetValueAsync(healthStatus);


        Debug.Log("exited UpdatePlayerDemographicData");

        /*{
            isMale: isMale,
            birthday: birthday,
            healthStatus: healthStatus,
            technologyUsageLevel: technologyUsageLevel,
            hasSibling: hasSibling
        });*/
        
        yield return new WaitForSeconds(1);
        
    }

    private void InitializeFirebase()
    {
        Debug.Log("Setting up Firebase Auth");
        //Set the authentication instance object
        auth = FirebaseAuth.DefaultInstance;
        databaseReference = FirebaseDatabase.DefaultInstance.RootReference;
    }
    
    //Function for the login button
    public void LoginButton()
    {
        //Call the login coroutine passing the email and password
        StartCoroutine(Login(emailLoginField.text, passwordLoginField.text));
    }
    
    //Function for the register button
    public void RegisterButton()
    {
        //Call the register coroutine passing the email, password, and username
        StartCoroutine(Register(emailRegisterField.text, passwordRegisterField.text));
    }

    private IEnumerator Login(string _email, string _password)
    {
        //Call the Firebase auth signin function passing the email and password
        var LoginTask = auth.SignInWithEmailAndPasswordAsync(_email, _password);
        //Wait until the task completes
        yield return new WaitUntil(predicate: () => LoginTask.IsCompleted);

        if (LoginTask.Exception != null)
        {
            //If there are errors handle them
            Debug.LogWarning(message: $"Failed to register task with {LoginTask.Exception}");
            FirebaseException firebaseEx = LoginTask.Exception.GetBaseException() as FirebaseException;
            AuthError errorCode = (AuthError)firebaseEx.ErrorCode;

            string message = "Login Failed!";
            switch (errorCode)
            {
                case AuthError.MissingEmail:
                    message = "Missing Email";
                    break;
                case AuthError.MissingPassword:
                    message = "Missing Password";
                    break;
                case AuthError.WrongPassword:
                    message = "Wrong Password";
                    break;
                case AuthError.InvalidEmail:
                    message = "Invalid Email";
                    break;
                case AuthError.UserNotFound:
                    message = "Account does not exist";
                    break;
            }
            warningLoginText.text = message;
        }
        else
        {
            //User is now logged in
            //Now get the result
            User = LoginTask.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})", User.DisplayName, User.Email);
            warningLoginText.text = "";
            confirmLoginText.text = "Logged In";

            _sessionEmail = _email;
            
            GameMonitor.instance.UserLogin(_sessionEmail, false);
        }
    }

    private IEnumerator Register(string _email, string _password)
    {
        /*if (_username == "")
        {
            //If the username field is blank show a warning
            warningRegisterText.text = "Missing Username";
        }
        else*/
        
        /*if(passwordRegisterField.text != passwordRegisterVerifyField.text)
        {
            //If the password does not match show a warning
            warningRegisterText.text = "Password Does Not Match!";
        }
        else 
        {*/
        
            //Call the Firebase auth signin function passing the email and password
            var RegisterTask = auth.CreateUserWithEmailAndPasswordAsync(_email, _password);
            //Wait until the task completes
            yield return new WaitUntil(predicate: () => RegisterTask.IsCompleted);

            if (RegisterTask.Exception != null)
            {
                //If there are errors handle them
                Debug.LogWarning(message: $"Failed to register task with {RegisterTask.Exception}");
                FirebaseException firebaseEx = RegisterTask.Exception.GetBaseException() as FirebaseException;
                AuthError errorCode = (AuthError)firebaseEx.ErrorCode;

                string message = "Register Failed!";
                switch (errorCode)
                {
                    case AuthError.MissingEmail:
                        message = "Missing Email";
                        break;
                    case AuthError.MissingPassword:
                        message = "Missing Password";
                        break;
                    case AuthError.WeakPassword:
                        message = "Weak Password";
                        break;
                    case AuthError.EmailAlreadyInUse:
                        message = "Email Already In Use";
                        break;
                }
                warningRegisterText.text = message;
            }
            else
            {
                //User has now been created
                //Now get the result
                User = RegisterTask.Result;
                _sessionEmail = _email;

                /*if (User != null)
                {
                    //Create a user profile and set the username
                    UserProfile profile = new UserProfile{DisplayName = _username};

                    //Call the Firebase auth update user profile function passing the profile with the username
                    var ProfileTask = User.UpdateUserProfileAsync(profile);
                    //Wait until the task completes
                    yield return new WaitUntil(predicate: () => ProfileTask.IsCompleted);

                    if (ProfileTask.Exception != null)
                    {
                        //If there are errors handle them
                        Debug.LogWarning(message: $"Failed to register task with {ProfileTask.Exception}");
                        FirebaseException firebaseEx = ProfileTask.Exception.GetBaseException() as FirebaseException;
                        AuthError errorCode = (AuthError)firebaseEx.ErrorCode;
                        warningRegisterText.text = "Username Set Failed!";
                    }
                    else
                    {
                        //Username is now set
                        //Now return to login screen
                        
                        ///UIManager.instance.LoginScreen();
                        warningRegisterText.text = "";
                    }
                }*/
                
                GameMonitor.instance.UserLogin(_sessionEmail, true);
            }
        ///}
    }

}
