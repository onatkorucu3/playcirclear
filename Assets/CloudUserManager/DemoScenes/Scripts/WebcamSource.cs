﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Text;


public class WebcamSource : MonoBehaviour, ImageSourceInterface
{
	[Tooltip("Whether the web-camera output needs to be flipped horizontally or not.")]
	public bool flipHorizontally = false;

	[Tooltip("Selected web-camera name, if any.")]
	public string webcamName;

	// the web-camera texture
	private WebCamTexture webcamTexture;

	// whether the output aspect ratio is set
	private bool isTextureResolutionSet = false;
	private bool isRotationAdjusted = false;
	
	public Quaternion baseRotation;

	private RawImage rawImage;

	private static int webCamTextureRotation;
	
	void Start()
	{
	}

	public WebCamTexture GetWebCamTexture()
	{
		if (webcamTexture != null)
		{
			Debug.Log("inside GetWebCamTexture");
		}
		return webcamTexture;
	}

	public virtual void Awake ()
	{
		FindFrontCamera();
		
	}

	public void FindFrontCamera()
	{
		WebCamDevice[] devices = WebCamTexture.devices;

		if (devices != null && devices.Length > 0)
		{
			// print available webcams
			StringBuilder sbWebcams = new StringBuilder();
			sbWebcams.Append("Available webcams:").AppendLine();

			foreach(WebCamDevice device in devices)
			{
				sbWebcams.Append(device.name).AppendLine();
			}

			Debug.Log(sbWebcams.ToString());

			// get 1st webcam name, if not set
			if(string.IsNullOrEmpty(webcamName))
			{
				foreach (var webCamDevice in devices)
				{
					if (webCamDevice.isFrontFacing)
					{
						webcamName = webCamDevice.name;
					}
				}
				
				///webcamName = devices[0].name;
			}

			// create webcam tex
			webcamTexture = new WebCamTexture(webcamName);
			
			OnApplyTexture(webcamTexture);

			isTextureResolutionSet = false;
			
			Debug.Log("videoRotationAngle in WebcamSource is: " + webcamTexture.videoRotationAngle);

		}

		if (flipHorizontally && transform.localScale.x > 0)
		{
			Vector3 scale = transform.localScale;
			transform.localScale = new Vector3(-scale.x, scale.y, scale.z);
			Debug.Log("X Scale in WebcamSource is: " + transform.localScale.x);

		}

		if (HasCamera())
		{
			webcamTexture.Play();
		}
	}

	void Update()
	{
		if (!isTextureResolutionSet && webcamTexture != null && webcamTexture.isPlaying)
		{
			SetAspectRatio(webcamTexture.width, webcamTexture.height);

			isTextureResolutionSet = true;
		}
		
		///
		///transform.rotation = baseRotation * Quaternion.AngleAxis(webcamTexture.videoRotationAngle, Vector3.up);
		///

		///if (!isRotationAdjusted)
		///{
			AdjustRotation();
		///	isRotationAdjusted = true;
		///}
		
		///
	}


	void OnDisable()
	{
		if(webcamTexture)
		{
			webcamTexture.Stop();
			webcamTexture = null;
		}
	}


	/// <summary>
	/// Gets the image as texture2d.
	/// </summary>
	/// <returns>The image.</returns>
	public Texture2D GetImage()
	{
		Texture2D snap = new Texture2D(webcamTexture.width, webcamTexture.height, TextureFormat.ARGB32, false);

		if (webcamTexture)
		{
			snap.SetPixels(webcamTexture.GetPixels());
			snap.Apply();

			if (flipHorizontally)
			{
				snap = CloudTexTools.FlipTexture(snap);
			}
		}

		return snap;
	}


	// Check if there is web camera
	public bool HasCamera()
	{
		return webcamTexture && !string.IsNullOrEmpty(webcamTexture.deviceName);
	}


	public void OnApplyTexture(Texture tex)
    { 
	    rawImage = GetComponent<RawImage>();
        if (rawImage)
        {
	        ///
	        int rotAngle = -webcamTexture.videoRotationAngle;
	        while( rotAngle < 0 ) rotAngle += 360;
	        while( rotAngle > 360 ) rotAngle -= 360;
	        ///
	        
	        ///
	        //CanvasRenderer renderer = GetComponent<CanvasRenderer>();
	        //renderer.GetMaterial().mainTexture = webcamTexture;
	        ///baseRotation = transform.rotation;
	        ///webcamTexture.Play();

	        //AdjustRotation();
	        
	        ///

	        rawImage.texture = tex;

	        //rawImage.material.mainTexture = tex;
        }
    }

	private void AdjustRotation()
	{
		int orient = webcamTexture.videoRotationAngle;
		//orient = 90;
		
		//Debug.Log("localEulerAngles inside WebcamSource.AdjustRootation() before is: " + rawImage.rectTransform.localEulerAngles);

		rawImage.rectTransform.localEulerAngles = new Vector3(0,0, orient);
		//Debug.Log("orient inside WebcamSource.AdjustRootation() is: " + orient);
		//Debug.Log("localEulerAngles inside WebcamSource.AdjustRootation() after is: " + rawImage.rectTransform.localEulerAngles);
		webCamTextureRotation = orient;
	}

	public static int GetVideoRotationAngle()
	{
		return webCamTextureRotation;
	}

	public void SetAspectRatio(int width, int height)
    {
        AspectRatioFitter ratioFitter = GetComponent<AspectRatioFitter>();
        if (ratioFitter)
        {
            ratioFitter.aspectRatio = (float)width / (float)height;
            //ratioFitter.aspectRatio = 0.5f;
        }
    }
}
