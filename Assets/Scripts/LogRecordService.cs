using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace DefaultNamespace
{
    public class LogRecordService : MonoBehaviour //WARNING: Do not Debug.Log() inside this class, it might create endless loop.
    {
        #region SingletonImplementation
        
        public static LogRecordService instance;
    
        void Awake()
        {
            if (instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
            }
        }
        
        #endregion

        const int INITIAL_CHARACTER_CAPACITY = 15000;
        const int MAXIMUM_NUMBER_OF_RECORDED_LOGS = 700;
        const int MAXIMUM_NUMBER_OF_RECORDED_ERROR_LOGS = 150;
        const int MAXIMUM_NUMBER_OF_RECORDED_DISTINCT_LOGS = 300;
        
        private List<LogEntry> _logEntries;
        private List<LogEntry> _errorLogEntries;
        private List<LogEntry> _logEntriesWithoutDuplicates;
        
        private StringBuilder _stringBuilder;
        private LogEntryComparer _logEntryComparer;

        public void Start()//WARNING: Some logs thrown at the beginning may not be recorded
                           //because the execution order of nonLazy Services are currently random.
        {
            _logEntries = new List<LogEntry>();
            _errorLogEntries = new List<LogEntry>();
            _logEntriesWithoutDuplicates = new List<LogEntry>();

            _logEntryComparer = new LogEntryComparer();
            
            Application.logMessageReceivedThreaded += HandleLogMessageReceived;
            
            SROptions.OnCopyAllLogsButtonPressed += HandleCopyAllLogsButtonPressed;
            SROptions.OnCopyAllDistinctLogsButtonPressed += HandleCopyAllDistinctLogsButtonPressed;
            SROptions.OnCopyAllErrorLogsButtonPressed += HandleCopyAllErrorLogsButtonPressed;
        }

        public void HandleLogMessageReceived(string logString, string stackTrace, LogType type)
        {
            //#if !UNITY_EDITOR //You can make the functions only work outside UNITY_EDITOR
            //to get a small performance boost while working on other stuff.
            
            #if (DEVELOPMENT_BUILD || UNITY_EDITOR) //For better performance in release builds
            LogEntry newLogEntry = new LogEntry(logString, stackTrace, type);
            
            AddLogToLogEntries(newLogEntry);
            AddLogToDistinctLogEntries(newLogEntry);
            AddLogToErrorLogEntries(newLogEntry);
            #endif
            
            //#endif
        }

        private void AddLogToLogEntries(LogEntry logEntry)
        {
            _logEntries.Add(logEntry);

            if (_logEntries.Count > MAXIMUM_NUMBER_OF_RECORDED_LOGS)
            {
                _logEntries.RemoveAt(0);
            }
        }
        
        private void AddLogToErrorLogEntries(LogEntry logEntry)
        {
            if(logEntry.Type != LogType.Error) { return; }
            
            _errorLogEntries.Add(logEntry);
            
            if (_errorLogEntries.Count > MAXIMUM_NUMBER_OF_RECORDED_ERROR_LOGS)
            {
                _errorLogEntries.RemoveAt(0);
            }
        }

        private void AddLogToDistinctLogEntries(LogEntry logEntry)
        {
            _logEntriesWithoutDuplicates.Add(logEntry);
            _logEntriesWithoutDuplicates = _logEntriesWithoutDuplicates.ToArray().Distinct(_logEntryComparer).ToList();
            //ToArray() is used to solve "Collection was modified; enumeration operation may not execute" error.
            
            if (_logEntriesWithoutDuplicates.Count > MAXIMUM_NUMBER_OF_RECORDED_DISTINCT_LOGS)
            {
                _logEntriesWithoutDuplicates.RemoveAt(0);
            }
        }

        private void HandleCopyAllLogsButtonPressed()
        {
            CopyLogEntryListToClipboard(_logEntries, false);
        }
        
        private void HandleCopyAllErrorLogsButtonPressed()
        {
            CopyLogEntryListToClipboard(_errorLogEntries);
        }
        
        private void HandleCopyAllDistinctLogsButtonPressed()
        {
            CopyLogEntryListToClipboard(_logEntriesWithoutDuplicates);
        }
        
        private void CopyLogEntryListToClipboard(List<LogEntry> logEntries, bool willIncludeStackTrace = true)
        {
            _stringBuilder = new StringBuilder(INITIAL_CHARACTER_CAPACITY);

            foreach (LogEntry logEntry in logEntries)
            {
                _stringBuilder.Append("\n" + logEntry.LogString + "\n");
                
                if(!willIncludeStackTrace) { continue; }
                _stringBuilder.Append(logEntry.StackTrace);
            }

            GUIUtility.systemCopyBuffer = _stringBuilder.ToString();
        }
        
        
    }
}