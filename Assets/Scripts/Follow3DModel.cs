﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Follow3DModel : MonoBehaviour
{
    [SerializeField] private Transform _model;
    [SerializeField] private Vector3 _offset;
    [SerializeField] private TextMeshProUGUI _modelNameTMP;

    private Renderer _renderer;
    
    private void Awake()
    {
        if (_model)
        {
            _renderer = _model.gameObject.GetComponentInChildren<Renderer>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!_model)
        {
            Debug.Log("model missing");
            gameObject.SetActive(false);
        }
        else
        {
            transform.position = _model.transform.position + _offset;
            //transform.position = new Vector3(transform.position.x, transform.position.y, 40);
        }

        ToggleNameTMPVisibility();
    }

    private void ToggleNameTMPVisibility()
    {
        if (!_renderer.enabled)
        {
            _modelNameTMP.enabled = false;
        }
        else
        {
            _modelNameTMP.enabled = true;
        }
    }
}

