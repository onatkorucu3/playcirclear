﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettings : MonoBehaviour
{
    public int volume; //audio level

    private static GameSettings _instance;

    public static GameSettings Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = FindObjectOfType<GameSettings>();
            }

            return _instance;
        }
    }
    
    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
        
        //DontDestroyOnLoad(gameObject);

    }
    
    
}
