﻿#if UNITY_EDITOR
using System;
using System.Linq;
using UnityEditor;

static class JenkinsBuilder {
 [MenuItem("Auto Builder/Build Android")]
 public static void PerformBuildAndroid()
 {
  //var path = string.Format("Builds/MacOS/{0:yyyy-MM-dd-hh-mm-ss}", DateTime.Now);
  //var path = string.Format("\"~/Builds/MacOS/First\"");
  var path = string.Format("\"$WORKSPACE/PlayCircleAR/test.apk\"");

  if (UnityEditorInternal.InternalEditorUtility.inBatchMode)
  {
   path = GetBatchOutputPath();

   if (path == null)
   {
    throw new ArgumentException("-output is not set");
   }
  }

  PerformBuild(path, BuildTarget.Android);
 }
 static void PerformBuild(string path, BuildTarget target)
 {
  var scenes = EditorBuildSettings.scenes.Where(v=> v.enabled).Select(v=> v.path);
  BuildPipeline.BuildPlayer(scenes.ToArray(), path, target, BuildOptions.None);
 }

 static string GetBatchOutputPath()
 {
  var args = Environment.GetCommandLineArgs();
  for (int ii = 0; ii <args.Length; ++ii)
  {
   if (args[ii].ToLower() == "-output")
   {
    if (ii + 1 <args.Length)
    {
     return args[ii + 1];
    }
   }
  }

  return null;
 }
}
#endif