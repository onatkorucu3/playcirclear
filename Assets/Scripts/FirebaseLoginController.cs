﻿using System;
using System.Collections;
using System.Collections.Generic;
using Firebase.Auth;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FirebaseLoginController : MonoBehaviour
{
    private FirebaseAuth auth;

    [SerializeField] private InputField _emailInputField;
    [SerializeField] private InputField _passwordInputField;

    public void SignInUser()
    {
        // worked with "onatkorucu@hotmail.com" && "onatONAT1234*"
        auth = FirebaseAuth.DefaultInstance;

        auth.SignInWithEmailAndPasswordAsync(_emailInputField.text,_passwordInputField.text).ContinueWith(task => {
            if (task.IsCanceled) {
                Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted) {
                Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                return;
            }

            Firebase.Auth.FirebaseUser newUser = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);
        });
    }
}
