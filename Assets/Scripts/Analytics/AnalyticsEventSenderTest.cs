using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UI;

namespace DefaultNamespace
{
    public class AnalyticsEventSenderTest : MonoBehaviour
    {
        [SerializeField] private Button playButton;
        
        private void OnEnable()
        {
            Debug.Log("listener will be added");
            playButton.onClick.AddListener(SendUIEventData);
            Debug.Log("listener added");
        }
        
        private void OnDisable()
        {
            playButton.onClick.RemoveListener(SendUIEventData);
            Debug.Log("listener removed");
        }
        
        /*private void Awake()
        {
            playButton.onClick.AddListener(SendUIEventData);
        }
        
        private void OnDestroy()
        {
            playButton.onClick.RemoveListener(SendUIEventData);
        }*/

        public void SendUIEventData()
        {
            AnalyticsResult analyticsResult = Analytics.CustomEvent("PlayButtonClicked");
            Debug.Log(analyticsResult);

            PlayerLoseEventData(); //TODO: Call this somewhere else

            ComplexPlayerLoseEventData(); //TODO: Call this somewhere else
        }
        
        public void PlayerLoseEventData()
        {
            Vector3 playerDeathPosition = transform.position;
            AnalyticsResult analyticsResult = Analytics.CustomEvent("PlayerLose", playerDeathPosition);
            Debug.Log(analyticsResult);
        }
        
        public void ComplexPlayerLoseEventData()
        {
            Vector3 playerDeathPosition = transform.position;
            int score = new int();

            Dictionary<string, object> complexEvent = new Dictionary<string, object>();
            
            complexEvent.Add("playerDeathPosition", playerDeathPosition);
            complexEvent.Add("score", score);
            
            AnalyticsResult analyticsResult = Analytics.CustomEvent("ComplexPlayerLose", complexEvent);
            Debug.Log(analyticsResult);
        }
    }
}