﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class AnalyticsEventSender : MonoBehaviour
{
    GameMonitor Monitor => GameMonitor.instance;

    #region SINGLETON

    public static AnalyticsEventSender instance;
    
    void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
    
    #endregion

    void Start () {
        AssignDelegates();
    }
    
    void AssignDelegates() {
        Monitor.OnApplicationOpen += HandleOnApplicationOpen;
        
        Monitor.OnUserLogin += HandleOnUserLogin;
        Monitor.OnPlayerDataEntered += HandleOnPlayerDataEntered;
        
        Monitor.OnGameLevelStart += HandleOnGameLevelStarted;
        
        Monitor.OnFaceDetectionStepCompleted += HandleOnFaceDetectionStepCompleted;
        Monitor.OnFaceDetectionLevelCompleted += HandleOnFaceDetectionLevelCompleted;
        Monitor.OnFaceDetectionLevelFailed += HandleOnFaceDetectionLevelFailed;

        Monitor.OnClassificationStepCompleted += HandleOnClassificationStepCompleted;
        Monitor.OnClassificationStepDecayed += HandleOnClassificationStepDecayed;
        Monitor.OnClassificationLevelCompleted += HandleOnClassificationLevelCompleted;
        Monitor.OnClassificationLevelFailed += HandleOnClassificationLevelFailed;
    }
    
    #region PLAYER_INFO
    private void HandleOnApplicationOpen(string userEmail, DateTime timestamp, string deviceUniqueIdentifier, string deviceModel)
    {
        AnalyticsResult analyticsResult = Analytics.CustomEvent("ApplicationOpened", new Dictionary<string, object> {
            { "userEmail", userEmail },
            { "timestamp", timestamp },
            { "deviceUniqueIdentifier", deviceUniqueIdentifier },
            { "deviceModel", deviceModel }
        });

        Debug.Log("AnalyticsResult ApplicationOpened: " + analyticsResult   
                                                                  + "\n userEmail: " + userEmail
                                                                  + "\n timestamp: " + timestamp
                                                                  + "\n deviceUniqueIdentifier: " + deviceUniqueIdentifier
                                                                  + "\n deviceModel: " + deviceModel);
    }
    
    void HandleOnUserLogin (string userEmail, bool isFirstLogin) {
        
        AnalyticsResult analyticsResult = Analytics.CustomEvent("UserLogin", new Dictionary<string, object> {
            { "userEmail", userEmail },
            { "timestamp", DateTime.Now },
            { "isFirstLogin", isFirstLogin }
        });

        Debug.Log("AnalyticsResult UserLogin: " + analyticsResult   
                                                + "\n userEmail: " + userEmail
                                                + "\n timestamp: " + DateTime.Now
                                                + "\n isFirstLogin: " + isFirstLogin);
    }
    
    void HandleOnPlayerDataEntered (string userEmail, int siblingCount, bool isMale, DateTime birthDate, int technologyUsageLevel, string healthStatus) {
        AnalyticsResult analyticsResult = Analytics.CustomEvent("PlayerDataEntered", new Dictionary<string, object> {
            { "userEmail", userEmail },
            { "isMale", isMale },
            { "siblingCount", siblingCount },
            { "birthDate", birthDate },
            { "technologyUsageLevel", technologyUsageLevel },
            { "healthStatus", healthStatus }
        });

        Debug.Log("AnalyticsResult PlayerDataEntered: " + analyticsResult   
                                                        + "\n userEmail: " + userEmail
                                                        + "\n isMale: " + isMale
                                                        + "\n siblingCount: " + siblingCount
                                                        + "\n birthDate: " + birthDate
                                                        + "\n technologyUsageLevel: " + technologyUsageLevel
                                                        + "\n healthStatus: " + healthStatus);
    }
    #endregion
    
    #region LEVEL_STARTED
    void HandleOnGameLevelStarted (DateTime dateTime, string userID, int levelNumber) {
        AnalyticsResult analyticsResult = Analytics.CustomEvent("GameLevelStarted", new Dictionary<string, object> {
            { "levelID", dateTime },
            { "userID",  userID },
            { "levelNumber",  levelNumber }
        });

        Debug.Log("AnalyticsResult GameLevelStarted: " + analyticsResult   
                                                  + "\n levelID: " + dateTime
                                                  + "\n userID: " + userID
                                                  + "\n levelNumber: " + levelNumber);
    }
    #endregion
    
    #region CLASSIFICATION
    private void HandleOnClassificationStepCompleted(string userEmail, string gameIdTimestamp, int lastPlayedLevel, string classifiableObject, float stepCompletionTime)
    {
        AnalyticsResult analyticsResult = Analytics.CustomEvent("ClassificationStepCompleted", new Dictionary<string, object> {
            { "userEmail", userEmail },
            { "gameIdTimestamp", gameIdTimestamp },
            { "lastPlayedLevel", lastPlayedLevel },
            { "classifiableObject", classifiableObject },
            { "stepCompletionTime", stepCompletionTime }
        });

        Debug.Log("AnalyticsResult ClassificationStepCompleted: " + analyticsResult   
                                                                 + "\n userEmail: " + userEmail
                                                                 + "\n gameIdTimestamp: " + gameIdTimestamp
                                                                 + "\n lastPlayedLevel: " + lastPlayedLevel
                                                                 + "\n classifiableObject: " + classifiableObject
                                                                 + "\n stepCompletionTime: " + stepCompletionTime);
    }
    
    private void HandleOnClassificationStepDecayed(string userEmail, string gameIdTimestamp, int lastPlayedLevel, string decayedObject)
    {
        AnalyticsResult analyticsResult = Analytics.CustomEvent("ClassificationStepDecayed", new Dictionary<string, object> {
            { "userEmail", userEmail },
            { "gameIdTimestamp", gameIdTimestamp },
            { "lastPlayedLevel", lastPlayedLevel },
            { "decayedObject", decayedObject },
        });

        Debug.Log("AnalyticsResult ClassificationStepDecayed: " + analyticsResult   
                                                                   + "\n userEmail: " + userEmail
                                                                   + "\n gameIdTimestamp: " + gameIdTimestamp
                                                                   + "\n lastPlayedLevel: " + lastPlayedLevel
                                                                   + "\n decayedObject: " + decayedObject
        );
    }

    private void HandleOnClassificationLevelCompleted(string userEmail, string gameIdTimestamp, int lastPlayedLevel, 
        List<string> grossClassifiedObjectList, 
        List<string> decayedObjectList, float levelCompletionTime)
    {
        AnalyticsResult analyticsResult = Analytics.CustomEvent("ClassificationLevelCompleted", new Dictionary<string, object> {
            { "userEmail", userEmail },
            { "gameIdTimestamp", gameIdTimestamp },
            { "lastPlayedLevel", lastPlayedLevel },
            { "grossClassifiedObjectList", ConvertListToString(grossClassifiedObjectList) },
            { "decayedObjectList", ConvertListToString(decayedObjectList) },
            { "levelCompletionTime", levelCompletionTime }
        });

        Debug.Log("AnalyticsResult ClassificationLevelCompleted: " + analyticsResult   
                                                                  + "\n userEmail: " + userEmail
                                                                  + "\n gameIdTimestamp: " + gameIdTimestamp
                                                                  + "\n lastPlayedLevel: " + lastPlayedLevel
                                                                  + "\n grossClassifiedObjectList: " + ConvertListToString(grossClassifiedObjectList)
                                                                   + "\n decayedObjectList: " + ConvertListToString(decayedObjectList)
                                                                   + "\n levelCompletionTime: " + levelCompletionTime
        );
    }

    private void HandleOnClassificationLevelFailed(string userEmail, string gameIdTimestamp, int lastPlayedLevel, List<string> grossClassifiedObjectList, List<string> decayedObjectList, float levelFailTime)
    {
        AnalyticsResult analyticsResult = Analytics.CustomEvent("ClassificationLevelFailed", new Dictionary<string, object> {
            { "userEmail", userEmail },
            { "gameIdTimestamp", gameIdTimestamp },
            { "lastPlayedLevel", lastPlayedLevel },
            { "grossClassifiedObjectList", ConvertListToString(grossClassifiedObjectList) },
            { "decayedObjectList", ConvertListToString(decayedObjectList) },
            { "levelFailTime", levelFailTime }
        });

        Debug.Log("AnalyticsResult ClassificationLevelFailed: " + analyticsResult   
                                                                   + "\n userEmail: " + userEmail
                                                                   + "\n gameIdTimestamp: " + gameIdTimestamp
                                                                   + "\n lastPlayedLevel: " + lastPlayedLevel
                                                                   + "\n grossClassifiedObjectList: " + ConvertListToString(grossClassifiedObjectList)
                                                                   + "\n decayedObjectList: " + ConvertListToString(decayedObjectList)
                                                                   + "\n levelFailTime: " + levelFailTime
        );
    }
    #endregion
    
    #region FACE_DETECTION
    private void HandleOnFaceDetectionStepCompleted(string userEmail, string gameIdTimestamp, int lastPlayedLevel, MimickedEmotion mimickedEmotion, float stepCompletionTime)
    {
        AnalyticsResult analyticsResult = Analytics.CustomEvent("FaceDetectionStepCompleted", new Dictionary<string, object> {
            { "userEmail", userEmail },
            { "gameIdTimestamp", gameIdTimestamp },
            { "lastPlayedLevel", lastPlayedLevel },
            { "mimickedEmotion", mimickedEmotion },
            { "stepCompletionTime", stepCompletionTime }
        });

        Debug.Log("AnalyticsResult FaceDetectionStepCompleted: " + analyticsResult   
                                                + "\n userEmail: " + userEmail
                                                + "\n gameIdTimestamp: " + gameIdTimestamp
                                                + "\n lastPlayedLevel: " + lastPlayedLevel
                                                + "\n mimickedEmotion: " + mimickedEmotion
                                                + "\n stepCompletionTime: " + stepCompletionTime);
    }
    
    private void HandleOnFaceDetectionLevelCompleted(string userEmail, string gameIdTimestamp, int lastPlayedLevel, float levelCompletionTime)
    {
        AnalyticsResult analyticsResult = Analytics.CustomEvent("FaceDetectionLevelCompleted", new Dictionary<string, object> {
            { "userEmail", userEmail },
            { "gameIdTimestamp", gameIdTimestamp },
            { "lastPlayedLevel", lastPlayedLevel },
            { "levelCompletionTime", levelCompletionTime }
        });

        Debug.Log("AnalyticsResult FaceDetectionLevelCompleted: " + analyticsResult   
                                                                 + "\n userEmail: " + userEmail
                                                                 + "\n gameIdTimestamp: " + gameIdTimestamp
                                                                 + "\n lastPlayedLevel: " + lastPlayedLevel
                                                                 + "\n levelCompletionTime: " + levelCompletionTime
                                                                 );
    }
    
    private void HandleOnFaceDetectionLevelFailed(string userEmail, string gameIdTimestamp, int lastPlayedLevel, float levelFailTime)
    {
        AnalyticsResult analyticsResult = Analytics.CustomEvent("FaceDetectionLevelFailed", new Dictionary<string, object> {
            { "userEmail", userEmail },
            { "gameIdTimestamp", gameIdTimestamp },
            { "lastPlayedLevel", lastPlayedLevel },
            { "levelFailTime", levelFailTime }
        });

        Debug.Log("AnalyticsResult FaceDetectionLevelFailed: " + analyticsResult   
                                                                 + "\n userEmail: " + userEmail
                                                                 + "\n gameIdTimestamp: " + gameIdTimestamp
                                                                 + "\n lastPlayedLevel: " + lastPlayedLevel
                                                                 + "\n levelFailTime: " + levelFailTime);
    }
    
    #endregion

    private string ConvertListToString(List<string> list)
    {
        string s = "";
        foreach (string name in list)
        {
            s += name;
            s += "-"; 
        }
        
        if (s.Length > 0)
        {
            s = s.Remove(s.Length - 1); // remove last char (-)
        }

        return s;
    }
}