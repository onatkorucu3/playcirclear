﻿using System;
using System.Collections;
using DefaultNamespace;
using TMPro;
using Unity.Collections;
using UnityEngine;

public class DistanceCalculator : MonoBehaviour
{
    [SerializeField, ReadOnly] private GameObject _container;
    
    [SerializeField, ReadOnly] private float _distance;

    [SerializeField, ReadOnly] private GameObject _checkmark;

    [SerializeField] private TextMeshProUGUI _modelNameTMP;
    
    public static event Action <string> OnObjectClassified;
    public static event Action <string> OnObjectDecayed;
    
    private bool isClassified;

    private string trackedTag;

    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine(nameof(Wait)); //Hacky fix for immediate level complete
        _distance = 5;//Hacky fix for immediate level complete
        trackedTag = GetTrackedTagInMission();
        
        string tag = FindContainerTag();
        _container = GameObject.FindGameObjectWithTag(tag);
        
        Transform parent = transform.parent;
        _checkmark = parent.Find("Checkmark").gameObject;
        
        InvokeRepeating("CalculateDistance", 0f, 1f);  //1s delay, repeat every 1s
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(3f);
    }

    // Update is called once per frame
    void Update()
    {
        if (_distance < 1.9f && !isClassified)
        {
            Debug.Log("ppp just  classified.");
            isClassified = true;
            
            Debug.Log("ppp trackedTag: "+trackedTag);
            Debug.Log("ppp gameObject.tag: "+gameObject.tag);

            if (trackedTag.Equals(gameObject.tag))
            {
                //ShowCheckmark();
                MakeModelNameGreen();
                OnObjectClassified(GetClassifiableObjectName());
            }
            else
            {
                MakeModelNameYellow();
                //OnNonScoreableObjectClassified(GetClassifiableObjectName()); //TODO: Decide if you want to analize non scorable classified objects.
            }
        }

        if (_distance > 2.2f && isClassified)
        {
            isClassified = false;
            //HideCheckmark();
            MakeModelNameWhite();
            
            if (trackedTag.Equals(gameObject.tag))
            {
                OnObjectDecayed(GetClassifiableObjectName());
            }
        }
    }
    
    string GetTrackedTagInMission()
    {
        if (MissionDetailsHolder.level.Equals("1") || MissionDetailsHolder.level.Equals("4"))
        {
            return "Vehicle";
        }

        if (MissionDetailsHolder.level.Equals("3") || MissionDetailsHolder.level.Equals("6"))
        {
            return "ForestAnimal";
        }

        return "BirdAnimal";
    }
    
    string FindContainerTag()
    {
        if (gameObject.tag == "Vehicle")
        {
            return "Garage";
        }
        if (gameObject.tag == "ForestAnimal")
        {
            return "Barn";
        }
        return "Coop";
    }
    
    void CalculateDistance() {
        _distance = Vector3.Distance (_container.transform.position, transform.position);
    }

    private string GetClassifiableObjectName()
    {
        Debug.Log("ppp inside GetClassifiableObjectName");
        string parentFullName = gameObject.transform.parent.name;
        Debug.Log("ppp parentFullName is:" + parentFullName);

        string classifiableObjectName = parentFullName.Remove(parentFullName.Length - 6);
        
        Debug.Log("ppp NameOfClassifiableObject is: " + classifiableObjectName);
        return classifiableObjectName;
    }

    private void MakeModelNameGreen()
    {
        Debug.Log("Green!" + _distance);
        _modelNameTMP.color = Color.green;
    }    
    
    private void MakeModelNameYellow()
    {
        Debug.Log("Yellow!" + _distance);
        _modelNameTMP.color = Color.yellow;
    }
    
    private void MakeModelNameWhite()
    {
        Debug.Log("White!");
        _modelNameTMP.color = Color.white;
    }
    
    private void HideCheckmark()
    {
        Debug.Log("Hide!");
        _checkmark.SetActive(false);
    }

    private void ShowCheckmark()
    {
        Debug.Log("Show!" + _distance);
        _checkmark.SetActive(true);
    }

    void OutputTime() {
        Debug.Log(Time.time);
    }
}
