﻿using System;
using DefaultNamespace;
using Unity.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScoreManager : MonoBehaviour
{
    private int _score;
    private int _targetScore;
    
    [ReadOnly] private float stepCompletionTime;
    [ReadOnly] private float levelCompletionTime;

    private void Awake()
    {
        stepCompletionTime = 0;
        levelCompletionTime = 0;
    }
    
    private void Update()
    {
        stepCompletionTime += Time.deltaTime;
    }
    
    private void OnEnable()
    {
        _targetScore = GetNeededScore();
        DistanceCalculator.OnObjectClassified += HandleOnObjectClassified;
        DistanceCalculator.OnObjectDecayed += HandleOnObjectDecayed;
    }

    private void OnDisable()
    {
        DistanceCalculator.OnObjectClassified -= HandleOnObjectClassified;
        DistanceCalculator.OnObjectDecayed -= HandleOnObjectDecayed;
    }
    
    private int GetNeededScore()
    {
        Debug.Log("MissionDetailsHolder.level is: "+MissionDetailsHolder.level);
        if (MissionDetailsHolder.level.Equals("1") || MissionDetailsHolder.level.Equals("2") ||
            MissionDetailsHolder.level.Equals("3"))
        {
            return 1;
        } 
        return 2;
    }

    private void HandleOnObjectClassified(string classifiedObjectName)
    {
        Debug.Log("ppp score before increase is: " + _score);
        Debug.Log("ppp _targetScore is: " + _targetScore);
        _score++;
        Debug.Log("Score Increased");

        bool isLevelComplete = _score >= _targetScore;

        Debug.Log("ppp isLevelComplete is: "+isLevelComplete);
        
        ClassificationCompleteAnalyticsData(classifiedObjectName, isLevelComplete);
        
        if (isLevelComplete)
        {
            CompleteGame();
        }
    }
    
    private void ClassificationCompleteAnalyticsData(string classifiedObjectName, bool isLevelComplete)
    {
        GameMonitor.instance.ClassificationStepCompleted(stepCompletionTime, classifiedObjectName);
        levelCompletionTime += stepCompletionTime;
        stepCompletionTime = 0;
        
        if (isLevelComplete)
        {
            GameMonitor.instance.ClassificationLevelCompleted(levelCompletionTime);
            levelCompletionTime = 0;
        }
    }

    private void HandleOnObjectDecayed(string decayedObjectName)
    {
        _score--;
        Debug.Log("Score Decreased");

        if (_score < 0) _score = 0;
        
        ClassificationDecayedAnalyticsData(decayedObjectName);
    }

    private void ClassificationDecayedAnalyticsData(string decayedObjectName)
    {
        GameMonitor.instance.ClassificationStepDecayed(decayedObjectName);
    }

    private void CompleteGame()
    {
        SceneManager.LoadScene("GameScene"); //WinScene
    }

    public void RestartScene()
    {
        Debug.Log("SCENE RESTARTED");
        SceneManager.LoadScene("SampleScene");
    }
}
