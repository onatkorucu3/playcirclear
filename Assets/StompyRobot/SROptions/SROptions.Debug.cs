
    using System;
    using System.ComponentModel;
    using UnityEngine;
    
    public partial class SROptions
    {
        public static event Action OnCopyAllLogsButtonPressed;
        public static event Action OnCopyAllErrorLogsButtonPressed;
        public static event Action OnCopyAllDistinctLogsButtonPressed;
    
        [Category("Logs")]
        public void CopyAllLogs()
        {
            OnCopyAllLogsButtonPressed();
        }   
        
        [Category("Logs")]
        public void CopyAllErrorLogs()
        {
            OnCopyAllErrorLogsButtonPressed();
        }   
    
        [Category("Logs")]
        public void CopyAllDistinctLogs()
        {
            OnCopyAllDistinctLogsButtonPressed();
        }
    
        [Category("Utilities")]
        public void ClearPlayerPrefs() {
            Debug.Log("Clearing PlayerPrefs"); 
            PlayerPrefs.DeleteAll();
        }
    
        [Category("Utilities")]
        public void CopyDeviceUniqueID() { 
            GUIUtility.systemCopyBuffer = SystemInfo.deviceUniqueIdentifier;
        }
    }