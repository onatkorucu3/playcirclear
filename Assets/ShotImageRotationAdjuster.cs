﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ShotImageRotationAdjuster : MonoBehaviour
{
    [SerializeField, ReadOnly] private WebCamTexture webcamTex;
    [SerializeField] private WebcamSource webcamSource;
    [SerializeField, ReadOnly] private RawImage _rawImage;

    private bool hasCamera;
    // Start is called before the first frame update
    void Start()
    {
        /*hasCamera = webcamSource != null && webcamSource.HasCamera(); //duplicate with CloudFaceDetector

        while (!hasCamera)
        {
            webcamSource.FindFrontCamera(); //Probably, Somehow rotates main video as well.
            hasCamera = webcamSource != null && webcamSource.HasCamera();
        }
        
        webcamTex = webcamSource.GetWebCamTexture();*/
        _rawImage = GetComponent<RawImage>();

        //Debug.Log("videoRotationAngle in ShotImageRotationAdjuster is: " + webcamTex.videoRotationAngle);
    }

    // Update is called once per frame
    void Update()
    {
        AdjustRotation();
    }
    
    private void AdjustRotation()
    {
        /*if(!webcamTex){
            Debug.Log("webcamTexture in ShotImageRotationAdjuster not found.");}
        
        if(!_rawImage){
            Debug.Log("_rawImage in ShotImageRotationAdjuster not found.");}
        
        if(!_rawImage.rectTransform){
            Debug.Log("_rawImage.rectTransform in ShotImageRotationAdjuster not found.");}
        
        int orient = webcamTex.videoRotationAngle;
        _rawImage.rectTransform.localEulerAngles = new Vector3(0,0, -orient); // should be "-" 
*/
        //Debug.Log("localEulerAngles inside ShotImageRotationAdjuster before is: " + _rawImage.rectTransform.localEulerAngles);

        int orient = WebcamSource.GetVideoRotationAngle();
        //Debug.Log("orient by static function is :" + orient);
        _rawImage.rectTransform.localEulerAngles = new Vector3(0,0, orient); // should be "-" 
        //Debug.Log("localEulerAngles inside ShotImageRotationAdjuster after is: " + _rawImage.rectTransform.localEulerAngles);

    }
}
