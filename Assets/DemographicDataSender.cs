﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class DemographicDataSender : MonoBehaviour
{

    [SerializeField] private AuthManager _authManager;

    [SerializeField] private InputField _healthStatus;
    
    [SerializeField] private InputField _birthDay;
    [SerializeField] private InputField _birthMonth;
    [SerializeField] private InputField _birthYear;

    [SerializeField] private Toggle _maleGenderToggle;
    [SerializeField] private Toggle _hasSiblingToggle;
    [SerializeField] private InputField _technologyUsageLevel;

    void Start()
    {
        _authManager = FindObjectOfType<AuthManager>();
    }
    
    public void SendDemographicData()
    {
        DateTime dateTime = new DateTime(int.Parse(_birthYear.text),int.Parse(_birthMonth.text), int.Parse(_birthDay.text));

        _authManager.SavePlayerDemographicData(
            _maleGenderToggle.isOn,
            _hasSiblingToggle.isOn,
            dateTime,
            int.Parse(_technologyUsageLevel.text),
            _healthStatus.text
            );

        GameMonitor.instance.PlayerDataEntered(
            _maleGenderToggle.isOn,
            Convert.ToInt32(_hasSiblingToggle.isOn), 
            dateTime, 
            int.Parse(_technologyUsageLevel.text), 
            _healthStatus.text);
    }
}
