﻿using Unity.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReturnToLevelScene : MonoBehaviour
{
    [ReadOnly] private float levelFailTime; //TODO: Move in score manager?
    
    void Update()
    {
        levelFailTime += Time.deltaTime;
    }
    
    public void GoBackButton()
    {
        Debug.Log("BACK BUTTON PRESSED IN CLASSIFICATION SCENE. levelFailTime is: " + levelFailTime);
        GameMonitor.instance.ClassificationLevelFailed(levelFailTime);
        levelFailTime = 0;
        SceneManager.LoadSceneAsync("LevelScene");
    }
}
