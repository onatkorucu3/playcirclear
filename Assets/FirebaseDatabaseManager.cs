﻿using System.Collections;
using System.Collections.Generic;
using Firebase.Database;
using UnityEngine;

public class FirebaseDatabaseManager : MonoBehaviour
{
    private DatabaseReference databaseReference;
    // Start is called before the first frame update
    void Start()
    {
        
        // Get the root reference location of the database.
        //databaseReference = FirebaseDatabase.DefaultInstance.RootReference;

        //writeNewUser("testUserId","testName","testMail@gmail.com");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    private void writeNewUser(string userId, string name, string email) {
        User user = new User(name, email);
        string json = JsonUtility.ToJson(user);

        databaseReference.Child("users").Child(userId).SetRawJsonValueAsync(json);
    }
    
}

public class User {
    public string username;
    public string email;

    public User() {
    }

    public User(string username, string email) {
        this.username = username;
        this.email = email;
    }
}
